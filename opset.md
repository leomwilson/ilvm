# ILVM Instruction Set

| ID | Hex | Name | Description | Registers Altered | Done? |
| -- | --- | ---- | ----------- | ----------------- | :---: |
| NOP | 0x00 | No-Op | Does nothing | | * |
| EXT | 0x01 | Exit | Exits the program with the code stored in D | | * |
| DBG | 0x02 | Debug | Prints "here" followed by a newline to the console | | * |
| DMP | 0x03 | Dump | Dumps the program's memory to the console | |  |
| PVM | 0x04 | Print VM | Prints a message determined by the VM, generally its name and other info | |  |
| EGG | 0x05 | Easter Egg | Performs some undocumented action, as determined by the VM | ? |  |
| RES | 0x06 | Reserved | Currently unused | |  |
| INC | 0x10 | Increment | Increments A, wrapping from 255 to 0 | A, H | * |
| DEC | 0x11 | Decrement | Decrements A, wrapping from 0 to 255 | A, H | * |
| ZRA | 0x12 | Zero A | Sets A to 0 | A | * |
| ZRB | 0x13 | Zero B | Sets B to 0 | B | * |
| ZRC | 0x14 | Zero C | Sets C to 0 | C | * |
| ZRD | 0x15 | Zero D | Sets D to 0 | D | * |
| CLE | 0x16 | Clear E | Empties E | E |  |
| CLF | 0x17 | Clear F | Empties F | F |  |
| RES | 0x18 | Reserved | Currently unused | |  |
| RES | 0x19 | Reserved | Currently unused | |  |
| ZRS | 0x1A | Zero Registers | Sets A-D to 0 | A, B, C, D | * |
| RES | 0x1B | Reserved | Currently unused | |  |
| INF | 0x1C | VM Info | Sets D to a VM ID | D |  |
| ONF | 0x1D | OS Info | Sets D to an OS ID | D |  |
| ADD | 0x20 | Add | Adds B to A, wrapping from 255 to 0 | A, H |  |
| SUB | 0x21 | Subtract | Subtracts B from A, wrapping from 0 to 255 | A, H |  |
| MUL | 0x22 | Multiply | Multiplies A by B and stores the result in A, truncating to the least significant eight bits | A, H |  |
| DIV | 0x23 | Divide | Divides A by B, stores the quotient in A, and stores the remainder in B | A, B, H |  |
| NEG | 0x24 | Negative | Stores the two's complement of A in A | A |  |
| AND | 0x25 | And | Stores the bitwise AND of A and D in D | D |  |
| ORR | 0x26 | Or | Stores the bitwise OR of A and D in D | D |  |
| XOR | 0x27 | Exclusive Or | Stores the bitwise XOR of A and D in D | D |  |
| LST | 0x28 | Left Shift | Shifts D left by A places and stores in D | D |  |
| LSW | 0x29 | Wrapping Left Shift | Shifts D left by A places, wrapping the left-most digits, and stores in D | D |  |
| RST | 0x2A | Logical Right Shift | Logically shifts D right by A places and stores in D | D |  |
| RSA | 0x2B | Arithmetic Right Shift | Arithmetically shifts D right by A places and stores in D |  |
| RSW | 0x2C | Wrapping Right Shift | Shifts D right by A places, wrapping the right-most digits, and stores in D | D |  |
| NOT | 0x2D | Not | Stores the one's complement (binary not) of D in D | D |  |
| PSA | 0x30 | Push A | Pushes A to E | E |  |
| PSB | 0x31 | Push B | Pushes B to E | E |  |
| PSC | 0x32 | Push C | Pushes C to E | E |  |
| PSD | 0x33 | Push D | Pushes D to E | E |  |
| RES | 0x34 | Reserved | Currently unused | |  |
| RES | 0x35 | Reserved | Currently unused | |  |
| PSG | 0x36 | Push G | Pushes the 8bit-truncated value of G to E | E |  |
| PSF | 0x37 | Push F | Pushes F to E | E |  |
| PPA | 0x38 | Pop A | Pops E into A | A, E |  |
| PPB | 0x39 | Pop B | Pops E into B | B, E |  |
| PPC | 0x3A | Pop C | Pops E into C | C, E |  |
| PPD | 0x3B | Pop D | Pops E into D | D, E |  |
| PPE | 0x3C | Pop E | Pops E, discarding the value | E |  |
| RES | 0x3D | Reserved | Currently unused | |  |
| RES | 0x3E | Reserved | Currently unused | |  |
| RES | 0x3F | Reserved | Currently unused | |  |
| JMP | 0x40 | Jump | Unconditionally pops F to G | F, G |  |
| JEQ | 0x41 | Jump if Equal | Pops F, setting G only if H carries the equality flag | F, G |  |
| JZR | 0x42 | Jump if Zero | Pops F, setting G only if H carries the zero flag | F, G |  |
| JWP | 0x43 | Jump if Wrapped | Pops F, setting G only if H carries the wrapping flag | F, G |  |
| JTR | 0x44 | Jump if Truncated | Pops F, setting G only if H carries the truncation flag | F, G |  |
| JEV | 0x45 | Jump if Even | Pops F, setting G only if H carries the even flag | F, G |  |
| JPT | 0x46 | Jump if Parity | Pops F, setting G only if H carries the parity flag | F, G |  |
| RES | 0x47 | Reserved | Currently unused | |  |
| JNV | 0x48 | Jump if Inverted | Pops F, setting G only if H carries the inverted flag | F, G |  |
| JAD | 0x49 | Jump if A and D | Pops F, setting G only if A & B is non-zero | F, G |  |
| RES | 0x4A | Reserved | Currently unused | |  |
| CMP | 0x4B | Compare | Subtracts B from A, discarding the result but setting H accordingly | H |  |
| NVH | 0x4C | Invert H | Inverts all flags in H | H |  |
| CLH | 0x4D | Clear H | Resets H to zero | H |  |
| JHM | 0x4E | Jump Home | Sets G to zero, thus jumping to the start of the program while mainaining its state | G |  |