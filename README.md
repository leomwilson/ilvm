# ILVM

The virtual machine upon which Ilang is built.

## Usage
`ilvm` reads and executes instructions from stdin.
`ilvm some_file.ilc` reads and executes instructions from a file.
There are no additional options.

## Program Behaviour

### Initialization
The eight registers, `A`-`H`, are initialized, the starting value of each being zero. `E`-`H` have special behaviour documented in here and in the instruction set.

### Execution
Each byte of the input file is read and executed. See the [Instruction Set](opset.md).

## End of file
Exits with the code stored in `D`.

## Registers

### Simple Registers
Registers `A`, `B`, `C`, and `D` are simple, 8-bit r/w registers generally treated as unsigned integers.

### Main Stack
`E` is a LIFO stack of 8-bit values which can be pushed from or popped to each of the simple registers.

### Loop Stack
`F` is a LIFO stack of instruction pointers (see below) which can be used to store locations for jumps.

### Instruction Pointer
`G` holds the pointer to the current instruction. It has no officially specified size, but a full implementation of the ILVM should make it sufficiently large that any reasonable program could be executed. This repo uses a C unsigned long integer for the instruction pointer. Note that the when moving data between `G` and `A`-`E`, pointers will often be truncated to eight bits. Often, such movement is unsupported.

### Conditional Flags
`H` is an 8-bit set of boolean flags which can be extracted individually using bitwise operators. The flags will be reset each time an instruction stores data in them. The bits are arranged as follows:
Bit | Flag | Description
0 | Equality | Set if the two values involved in an operation were equal
1 | Zero | Set if the operation resulted in zero
2 | Wrapping | Set if the operation wrapped around from 0 to 255 or 255 to 0
3 | Truncation | Set if non-zero bits were truncated in the stored result of the previous operation
4 | Even | Set if the last operation resulted in a remainder of zero
5 | Parity | Set if the binary result of the last operation had an odd number of ones
6 | Reserved | Currently unused but may be used in the future
7 | Inverted | Always set to 0, useful for detecting `NTH`