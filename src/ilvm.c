#include <stdio.h>

int main(int argc, char **argv) {
    FILE *file = (argc > 1) ? fopen(argv[1], "rb") : stdin;
    if (!file) {
        fprintf(stderr, "[ILVM] Could not open file: %s\n", argv[1]);
        return 2;
    }

    // read all bytes to byte array
    fseek(file, 0, SEEK_END);
    long size = ftell(file);
    rewind(file);
    unsigned char ins[size];
    fread(ins, size, 1, file);
    fclose(file);

    // set up everything
    unsigned char a = 0, b = 0, c = 0, d = 0; // main byte registers
    // TODO: make this not 256
    unsigned char e[256]; // main stack
    unsigned long eptr = 0; // stacks grow upwards
    unsigned char f[256]; // "loop stack"
    unsigned long fptr = 0;
    unsigned long iptr = 0; // G is based on this but not stored as a variable
    unsigned char h = 0; // current conditional flags

    while (1) {
        if (iptr >= size || ins[iptr] == 0x01) { // end of file or EXT
            break;
        }
        switch (ins[iptr]) {
            case 0x00: // NOP
                break;
            case 0x02: // DBG
                printf("here\n");
                break;
            case 0x10: // INC
                a++;
                break;
            case 0x11: // DEC
                a--;
                break;
            case 0x12: // ZRA
                a = 0;
                break;
            case 0x13: // ZRB
                b = 0;
                break;
            case 0x14: // ZRC
                c = 0;
                break;
            case 0x15: // ZRD
                d = 0;
                break;
            case 0x16: // CLE
                eptr = 0;
                // TODO: clear E
                break;
            case 0x17: // CLF
                fptr = 0;
                // TODO: clear F
                break;
            case 0x1A: // ZRS
                a = 0;
                b = 0;
                c = 0;
                d = 0;
                break;
            default:
                fprintf(stderr, "[ILVM] Invalid instruction: 0x%X\n", ins[iptr]);
                return 1;
        }
        iptr++;
    }

    return d;
}